# java-maven
Test project with:

* **Language:** Java
* **Package Manager:** Maven

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| Dependency Scanning | :white_check_mark: |
| License Management  | :white_check_mark: |
